/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.twoactivities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    // what screen we want to start
    public ActivityTestRule activityRule = new ActivityTestRule<>(MainActivity.class);
    // send button takes to next page?
    // text we enters appears on next page
   @Test
    public void testGoingToNextPage(){

    onView(withId(R.id.button_main)).perform(click());
    // on next page check lable
       onView(withId(R.id.text_header)).check(matches(isDisplayed()));


       onView(withId(R.id.button_second)).perform(click());
    }
    @Test
    public void testInputBox() {

       onView(withId(R.id.editText_main)).perform(typeText("Hello Jatin"));
        onView(withId(R.id.button_main)).perform(click());
            String j = "Hello Jatin";
        onView(withId(R.id.text_message)).check(matches(withText(j)));
    }




}
